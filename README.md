Wikibase docker ansible role
============================

Deploys an installation of ansible in a linux environment.

Requirements
------------

- 4 GB RAM
- Linux Environment
- docker and docker-compose installed


Role Variables
--------------

```yaml
# where configuration files are stored
wikibase_path: /srv/wikibase

# where data is stored (nothing is stored inside docker volumes)
wikibase_data_path: "{{ wikibase_path }}/data"

# administrator credentials
wikibase_admin_name: admin
wikibase_admin_pass: passpass
wikibase_admin_email: admin@example.com

# Wiki configuration
wg_site_name: "wikibase-docker"
wg_language_code: "en"
wg_shell_locale: "en_US.utf8"
wg_secret_key: "secret"

# database credentials
sql_db_user: wikiuser
sql_db_pass: sqlpass
sql_db_name: my_wiki
```


Dependencies
------------

docker and docker-compose


Example Playbook
----------------

```yaml
  - hosts: servers

    roles:
      - role: docker
        become: yes

      - role: wikibase
        become: yes
```


Customization
-------------

You can customize the wikibase settings by adding settings on the 
`templates/LocalSettings.php.template.j2` file.


License
-------

Unlicense
